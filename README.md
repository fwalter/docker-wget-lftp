# docker-wget-lftp

A tiny (Alpine-based) Docker image containing wget, lftp, and openssh-client (with SSL/TLS support).

## Getting Started

The image is available via: https://hub.docker.com/r/fwtu/docker-wget-lftp

```
docker run --rm -it docker-wget-lftp:latest sh
```

## License

This code is licensed under the MIT license. See [LICENSE](LICENSE) for details.
