FROM        alpine

# install openssl, wget, lftp and clean up afterwards
RUN         apk --update add openssl ca-certificates wget lftp openssh-client && \
            rm -rf /var/cache/apk/*

ENTRYPOINT  []
